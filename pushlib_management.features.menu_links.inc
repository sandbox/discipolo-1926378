<?php
/**
 * @file
 * pushlib_management.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function pushlib_management_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: shortcut-set-2:admin/content
  $menu_links['shortcut-set-2:admin/content'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'Find content',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: shortcut-set-2:admin/content/biblio/add
  $menu_links['shortcut-set-2:admin/content/biblio/add'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'admin/content/biblio/add',
    'router_path' => 'admin/content/biblio/add',
    'link_title' => 'add publication',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: shortcut-set-2:node/add
  $menu_links['shortcut-set-2:node/add'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'node/add',
    'router_path' => 'node/add',
    'link_title' => 'Add content',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
    // Exported menu link: shortcut-set-2:admin/content
  $menu_links['shortcut-set-2:admin/content'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'admin/content',
    'router_path' => 'admin/content',
    'link_title' => 'Manage Content',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
      // Exported menu link: shortcut-set-2:node/add/post
  $menu_links['shortcut-set-2:node/add/post'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'node/add/post',
    'router_path' => 'node/add/post',
    'link_title' => 'Manage Content',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
      // Exported menu link: shortcut-set-2:node/add/page
  $menu_links['shortcut-set-2:node/add/page'] = array(
    'menu_name' => 'shortcut-set-2',
    'link_path' => 'node/add/page',
    'router_path' => 'node/add/page',
    'link_title' => 'Manage Content',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add content');
  t('Find content');
  t('add publication');


  return $menu_links;
}
