<?php
/**
 * @file
 * pushlib_management.features.shortcut_set.inc
 */

/**
 * Implements hook_shortcut_default_shortcut_set().
 */
function pushlib_management_shortcut_default_shortcut_set() {
  $shortcut_sets = array();
  $shortcut_sets['shortcut-set-2'] = array(
    'set_name' => 'shortcut-set-2',
    'title' => 'Pushlib Shortcuts',
  );
  return $shortcut_sets;
}
