<?php
/**
 * @file
 * pushlib_management.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pushlib_management_views_api() {
  return array("api" => "3.0");
}
