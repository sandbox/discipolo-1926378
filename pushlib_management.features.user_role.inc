<?php
/**
 * @file
 * pushlib_management.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function pushlib_management_user_default_roles() {
  $roles = array();

  // Exported role: Website Editor.
  $roles['Website Editor'] = array(
    'name' => 'Website Editor',
    'weight' => '2',
  );

  return $roles;
}
